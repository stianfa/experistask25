﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (!(SupervisorGroup.CurrentSupervisors.Count > 0))
            {
                SupervisorGroup.CurrentSupervisors.Add(new Supervisor { Name = "John", Id = 1 });
                SupervisorGroup.CurrentSupervisors.Add(new Supervisor { Name = "Peter", Id = 2 });
                SupervisorGroup.CurrentSupervisors.Add(new Supervisor { Name = "Dolly", Id = 3 });
            }

            return View("Supervisorinfo");
        }

        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor sup)
        {
           
            if (ModelState.IsValid)
            {
                SupervisorGroup.CurrentSupervisors.Add(sup);
                return View("Confirmation", sup);
            }
            else
            {
                return View();
            }
            
        }

        [HttpGet]
        public IActionResult AllSupervisors()
        {
            return View(SupervisorGroup.CurrentSupervisors);
        }
    }
}
